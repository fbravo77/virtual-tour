import 'package:flutter/material.dart';
import './custom-dialog.dart' as customDialog;

class Axonompopup extends StatelessWidget {
  final String imgPath;
  final String aptoName;

  const Axonompopup(this.imgPath, this.aptoName);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;

    return customDialog.AlertDialog(
      backgroundColor: Colors.black45,
      contentPadding: const EdgeInsets.only(top: 10),
      title: Center(
          child: Text(
        aptoName,
        style: TextStyle(color: Colors.cyan, fontSize: 30),
      )),
      content: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: mediaQuery.height * 0.39,
            width: mediaQuery.width,
            child: Image.asset(
              "assets/images/axonom/$imgPath.jpg",
              fit: BoxFit.cover,
            ),
          ),
        ],
      ),
      actions: <Widget>[
        FlatButton(
          child: const Text('Ok', style: TextStyle(fontSize: 25)),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
