/***EJEMPLO PARA UN FUTURO DROPDOWN */

/*
import 'package:flutter/material.dart';
import '../myWebView.dart';
import 'axonompopup.dart';

enum WhyFarther { TourVirtual, Axonometrica }

class Dropdown extends StatelessWidget {
  final String imgPath;
  final String aptoName;

  Dropdown(this.imgPath, this.aptoName);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<WhyFarther>(
      onSelected: (WhyFarther result) {
        if (result == WhyFarther.Axonometrica) {
          showDialog<void>(
            context: context,
            barrierDismissible: false, // user must tap button!
            builder: (BuildContext context) {
              return Axonompopup(imgPath, aptoName);
            },
          );
        } else if (result == WhyFarther.TourVirtual) {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => MyWebView(
                    title: "Tour: $aptoName",
                    selectedUrl: "https://virtualtour2020.000webhostapp.com/",
                  )));
        }
      },
      icon: Icon(
        Icons.settings,
        color: Colors.white,
        size: 40,
      ),
      itemBuilder: (BuildContext context) => <PopupMenuEntry<WhyFarther>>[
        const PopupMenuItem<WhyFarther>(
          value: WhyFarther.TourVirtual,
          child: ListTile(
            leading: Icon(
              Icons.threed_rotation,
            ),
            title: Text('Tour Virtual'),
          ),
        ),
        const PopupMenuItem<WhyFarther>(
          value: WhyFarther.Axonometrica,
          child: ListTile(
            leading: Icon(
              Icons.zoom_out_map,
            ),
            title: Text('Axonometrica'),
          ),
        ),
      ],
    );
  }
}
*/
