import 'package:flutter/material.dart';

import 'package:carousel_slider/carousel_slider.dart';

class Carousel extends StatelessWidget {
  final double carouselHeight;
  final double carouselWidth;
  final List imgList;

  const Carousel(this.carouselHeight, this.carouselWidth, this.imgList);

  @override
  Widget build(BuildContext context) {
    // print(carouselHeight + " ancho: " + carouselWidth);
    return CarouselSlider(
      autoPlay: true,
      enlargeCenterPage: true,
      height: carouselHeight,
      items: imgList.map((i) {
        return Builder(
          builder: (BuildContext context) {
            return Container(
              width: carouselWidth,
              margin: const EdgeInsets.symmetric(horizontal: 1.0),
              child: Image.asset(
                i,
                fit: BoxFit.cover,
              ),
            );
          },
        );
      }).toList(),
    );
  }
}
