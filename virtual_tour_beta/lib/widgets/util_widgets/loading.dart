import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loading extends StatelessWidget {
  final String destination;

  const Loading(this.destination);

  static const String routeName = '/loading';
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);

    Future<void> loadingWidget() {
      return Future.delayed(Duration(seconds: 3),
          () => Navigator.pushReplacementNamed(context, destination));
    }

    loadingWidget();
    return SpinKitCubeGrid(
      color: Colors.white,
      size: 100,
    );
  }
}
