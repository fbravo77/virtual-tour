import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../widgets/util_widgets/carousel.dart';
import '../../widgets/info/leftHandDrawer.dart';
import '../appBarOpt.dart';

class Fachada extends StatelessWidget {
  static const String routeName = '/fachadas';

  final List imgList = [
    'assets/images/fachadas/fachada1.jpg',
    'assets/images/fachadas/fachada2.jpg',
    'assets/images/fachadas/fachada3.jpg',
    'assets/images/fachadas/fachada4.jpg'
  ];

  @override
  Widget build(BuildContext context) {
    //Only these orientations
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);

    final AppBarOpt appbar = const AppBarOpt("Fachadas");
    var mediaQuery = MediaQuery.of(context).size;

    return Scaffold(
        appBar: appbar,
        endDrawer: const LeftHandDrawer(),
        body: Column(
          children: [
            Container(
              height: 1,
              color: Colors.cyan,
            ),
            Carousel(
                mediaQuery.height -
                    appbar.preferredSize.height -
                    1 -
                    MediaQuery.of(context).padding.top,
                mediaQuery.width,
                imgList)
          ],
        ));
  }
}
