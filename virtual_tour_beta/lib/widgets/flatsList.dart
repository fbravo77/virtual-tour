import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../widgets/appBarOpt.dart';

import '../widgets/info/leftHandDrawer.dart';
import '../models/flat.dart';
import 'dropdownList/axonompopup.dart';
import 'myWebView.dart';

class FlatsList extends StatelessWidget {
  static const String routeName = '/flats';

  final List<Flat> flats;

  FlatsList(this.flats);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    var size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: const AppBarOpt("Apartamentos"),
      endDrawer: const LeftHandDrawer(),
      body: ListView.builder(
        itemCount: flats.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: size.height * 0.33,
            child: Stack(
              children: <Widget>[
                Container(
                  width: size.width,
                  child: Image.asset(
                    "assets/images/aptos/${flats[index].image}.jpg",
                    fit: BoxFit.fill,
                  ),
                ),
                Text(
                  flats[index].name,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 33),
                ),
                Container(
                  height: 3,
                  color: Colors.cyan,
                ),
                Align(
                  alignment: Alignment.center,
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: size.height * 0.0858,
                      ),
                      FlatButton(
                        onPressed: () {
                          showDialog<void>(
                            context: context,
                            builder: (BuildContext context) {
                              return Axonompopup(flats[index].axonometricas,
                                  flats[index].name);
                            },
                          );
                        },
                        child: Text(
                          "PLANTA AXONOMETRICA",
                          style: TextStyle(fontSize: size.height * 0.0264),
                        ),
                      ),
                      SizedBox(
                        height: size.height * 0.0396,
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) => MyWebView(
                                    title: "Tour: ${flats[index].name}",
                                    selectedUrl:
                                        "https://virtualtour2020.000webhostapp.com/",
                                  )));
                        },
                        child: Column(
                          children: <Widget>[
                            Container(
                              height: size.height * 0.0594,
                              child: Image.asset("assets/images/aptos/360.png",
                                  fit: BoxFit.fill),
                            ),
                            Text(
                              "TOUR VIRTUAL",
                              style: TextStyle(fontSize: size.height * 0.0231),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
