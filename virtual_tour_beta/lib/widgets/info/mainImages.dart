import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MainImages extends StatelessWidget {
  const MainImages();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    return Column(
      children: <Widget>[
        Container(
          height: 2,
          color: Colors.cyan,
        ),
        Flexible(
          child: Image.asset(
            "assets/images/aparmentsLogo.png",
          ),
        ),
        Flexible(
          flex: 3,
          child: Image.asset(
            "assets/images/mainImage.gif",
            fit: BoxFit.cover,
            width: MediaQuery.of(context).size.width,
          ),
        ),
      ],
    );
  }
}
