import 'package:flutter/material.dart';

import './credits.dart';

class ShowCredits extends StatelessWidget {
  const ShowCredits();

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton.extended(
      label: const Text(
        'Créditos',
        style: TextStyle(color: Colors.white),
      ),
      backgroundColor: Color.fromARGB(255, 0, 133, 135),
      icon: const Icon(Icons.info, color: Colors.white),
      onPressed: () {
        showDialog<void>(
          context: context,
          builder: (BuildContext context) {
            return const Credits();
          },
        );
      },
    );
  }
}
