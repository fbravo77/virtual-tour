import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../routes/Routes.dart';

class LeftHandDrawer extends StatelessWidget {
  const LeftHandDrawer();

  @override
  Widget build(BuildContext context) {
    _launchURL(String toMailId, String subject, String body) async {
      var url = 'mailto:$toMailId?subject=$subject&body=$body';
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    }

    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            child: const Text(""),
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/aparmentsLogo.png"),
                  fit: BoxFit.cover),
            ),
          ),
          ListTile(
            leading: const Icon(
              Icons.location_city,
              color: Colors.cyan,
            ),
            title: const Text('Apartamentos'),
            onTap: () {
              Navigator.pushReplacementNamed(context, Routes.flats);
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.home,
              color: Colors.cyan,
            ),
            title: const Text('Fachadas'),
            onTap: () {
              Navigator.pushReplacementNamed(context, Routes.fachadas);
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.bubble_chart,
              color: Colors.cyan,
            ),
            title: const Text('Amenidades'),
            onTap: () {
              Navigator.pushReplacementNamed(context, Routes.loading);
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.attach_money,
              color: Colors.cyan,
            ),
            title: const Text('Precios'),
            onTap: () {
              Navigator.pushReplacementNamed(context, Routes.precios);
            },
          ),
          ListTile(
              leading: const Icon(
                Icons.mail,
                color: Colors.cyan,
              ),
              title: const Text('Contáctanos'),
              onTap: () {
                _launchURL(
                    'loira_aptos@gmail.com',
                    'Contacto de apartamento Loira',
                    'Mucho gusto, por favor contactarme, deseo mas información.');
              }),
        ],
      ),
    );
  }
}
