import 'package:flutter/material.dart';

class Credits extends StatelessWidget {
  const Credits();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Center(
        child: Text(
          'Créditos',
          style: TextStyle(color: Colors.cyan),
        ),
      ),
      content: SingleChildScrollView(
        child: ListBody(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Image.asset(
                    "assets/images/logo.png",
                  ),
                ),
                Center(child: Text('Realizado por 3DSign 2020')),
              ],
            ),
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: const Text('Ok'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
