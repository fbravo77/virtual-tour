import 'package:flutter/material.dart';
import '../routes/Routes.dart';

class AppBarOpt extends StatelessWidget implements PreferredSizeWidget {
  final String title;

  const AppBarOpt(this.title);

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

  double get sizeAppBar {
    return preferredSize.height;
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        title,
        style: TextStyle(
          fontSize: 28,
        ),
      ),
      centerTitle: true,
      leading: new IconButton(
        icon: new Icon(Icons.arrow_back, color: Colors.white),
        onPressed: () => Navigator.pushReplacementNamed(context, Routes.main),
      ),
    );
  }
}
