import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import './../util_widgets/carousel.dart';
import '../../widgets/info/leftHandDrawer.dart';
import '../appBarOpt.dart';

class Amenidades extends StatelessWidget {
  static const String routeName = '/amenidades';

  final List imgList = [
    'assets/images/amenidades/amen1.jpg',
    'assets/images/amenidades/amen2.jpg',
    'assets/images/amenidades/amen3.jpg',
    'assets/images/amenidades/amen4.jpg',
    'assets/images/amenidades/amen5.jpg'
  ];

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);

    final AppBarOpt appbar = const AppBarOpt("Amenidades");
    var mediaQuery = MediaQuery.of(context).size;

    return Scaffold(
        appBar: appbar,
        endDrawer: const LeftHandDrawer(),
        body: Column(
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: 1,
                color: Colors.cyan,
              ),
            ),
            Carousel(
                mediaQuery.height -
                    appbar.preferredSize.height -
                    1 -
                    MediaQuery.of(context).padding.top,
                mediaQuery.width,
                imgList)
          ],
        ));
  }
}
