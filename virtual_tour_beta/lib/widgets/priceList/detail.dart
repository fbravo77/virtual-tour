import 'package:flutter/material.dart';

class Detail extends StatelessWidget {
  final String title;
  final String detail;
  final double height;

  Detail(this.title, this.detail, this.height);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (ctx, constraint) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontSize: height * 0.08),
            // style: TextStyle(fontSize: constraint.maxHeight * 0.08),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 1.0),
            child: Text(detail,
                style: TextStyle(
                  fontSize: height * 0.08,
                )),
          ),
        ],
      );
    });
  }
}
