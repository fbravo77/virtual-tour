import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../widgets/appBarOpt.dart';
import '../../widgets/info/leftHandDrawer.dart';
import '../../models/flat.dart';
import 'detail.dart';

class Prices extends StatelessWidget {
  static const String routeName = '/prices';
  final List<Flat> flats;

  Prices(this.flats);

  @override
  Widget build(BuildContext context) {
    _launchURL(String toMailId, String subject, String body) async {
      var url = 'mailto:$toMailId?subject=$subject&body=$body';
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    }

    //Only these orientations
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    final double height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBarOpt("Precios"),
      endDrawer: LeftHandDrawer(),
      body: ListView.builder(
        itemCount: flats.length,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            children: <Widget>[
              Container(
                height: height * 0.007,
                color: Colors.cyan,
              ),
              Container(
                height: height * 0.45,
                width: MediaQuery.of(context).size.width,
                child: Image.asset(
                  "assets/images/axonom/${flats[index].axonometricas}.jpg",
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                height: height * 0.007,
                color: Colors.cyan,
              ),
              Container(
                height: height * 0.1,
                child: Text(
                  flats[index].name,
                  style: TextStyle(fontSize: height * 0.06),
                ),
              ),
              Detail("Precios:", flats[index].price, height * 0.35),
              Detail("Dormitorios:", flats[index].dormitorios, height * 0.35),
              Detail("Baños:", flats[index].banios, height * 0.35),
              Detail("Parqueos:", flats[index].parqueos, height * 0.35),
              Detail("Balcones:", flats[index].balcones, height * 0.35),
              SizedBox(height: height * 0.003),
              Divider(),
              FlatButton(
                padding: EdgeInsets.only(
                    top: height * 0.007, bottom: height * 0.007),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.contact_mail,
                      size: height * 0.07,
                    ),
                    Text(
                      "Contactar",
                      style: TextStyle(fontSize: height * 0.035),
                    ),
                  ],
                ),
                onPressed: () => _launchURL(
                    'loira@gmail.com',
                    'Contacto de apartamento Loira',
                    'Mucho gusto, por favor contactarme, deseo mas información.'),
              ),
            ],
          );
        },
      ),
    );
  }
}
