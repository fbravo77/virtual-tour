import 'package:flutter/foundation.dart';

class Flat {
  final String id;
  final String urlPath;
  final String description;
  final String name;
  final String image;
  final String price;
  final String axonometricas;
  final String banios;
  final String dormitorios;
  final String parqueos;
  final String balcones;

  Flat({
    @required this.id,
    @required this.urlPath,
    @required this.description,
    @required this.name,
    @required this.image,
    @required this.price,
    @required this.axonometricas,
    @required this.banios,
    @required this.dormitorios,
    @required this.balcones,
    @required this.parqueos,
  });
}
