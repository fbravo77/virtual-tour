import 'package:virtual_tour_beta/widgets/util_widgets/loading.dart';

import '../widgets/priceList/prices.dart';
import '../widgets/amenidades/amenidades.dart';
import '../widgets/fachadas/fachada.dart';
import '../widgets/flatsList.dart';
import '../main.dart';

class Routes {
  static const String flats = FlatsList.routeName;
  static const String main = MyHomePage.routeName;
  static const String fachadas = Fachada.routeName;
  static const String amenidades = Amenidades.routeName;
  static const String precios = Prices.routeName;
  static const String loading = Loading.routeName;
}
