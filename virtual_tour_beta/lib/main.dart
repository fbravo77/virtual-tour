import 'package:flutter/material.dart';

import './widgets/amenidades/amenidades.dart';
import './widgets/fachadas/fachada.dart';
import './widgets/flatsList.dart';
import './widgets/info/mainImages.dart';
import './widgets/info/leftHandDrawer.dart';
import './widgets/info/showCredits.dart';
import './widgets/priceList/prices.dart';

import 'models/flat.dart';
import 'widgets/util_widgets/loading.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  static final List<Flat> flats = [
    Flat(
        id: 'apto1',
        name: 'Apartamento A',
        description: "Apartamento de muestra numero uno",
        urlPath: "wwww.3dsign.com",
        image: "apto1",
        price: "\$80.000",
        axonometricas: "apto1axo",
        balcones: "2 balcones",
        banios: "2 baños completos",
        parqueos: "2 parqueos",
        dormitorios: "2 dormitorios"),
    Flat(
        id: 'apto2',
        name: 'Apartamento B',
        description: "Apartamento de muestra numero dos",
        urlPath: "wwww.3dsign.com",
        image: "apto1",
        price: "\$119.000",
        axonometricas: "apto2axo",
        balcones: "2 balcones",
        banios: "2 baños completos",
        parqueos: "2 parqueos",
        dormitorios: "2 dormitorios"),
    Flat(
        id: 'apto3',
        name: 'Apartamento C',
        description: "Apartamento de muestra numero tres",
        urlPath: "wwww.3dsign.com",
        image: "apto1",
        price: "\$149.000",
        axonometricas: "apto3axo",
        balcones: "2 balcones",
        banios: "2 baños completos",
        parqueos: "2 parqueos",
        dormitorios: "2 dormitorios")
  ];

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'virtual tour',
      theme: ThemeData.dark(),
      home: MyHomePage(),
      routes: <String, WidgetBuilder>{
        FlatsList.routeName: (BuildContext context) => FlatsList(flats),
        MyHomePage.routeName: (BuildContext context) => MyHomePage(),
        Fachada.routeName: (BuildContext context) => Fachada(),
        Amenidades.routeName: (BuildContext context) => Amenidades(),
        Prices.routeName: (BuildContext context) => Prices(flats),
        Loading.routeName: (BuildContext context) =>
            Loading(Amenidades.routeName),
      },
    );
  }
}

class MyHomePage extends StatelessWidget {
  static const String routeName = '/main';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Menu",
          style: TextStyle(
            fontSize: 28,
          ),
        ),
        centerTitle: true,
      ),
      body: Container(
        child: const MainImages(),
      ),
      drawer: const LeftHandDrawer(),
      floatingActionButton: const ShowCredits(),
    );
  }
}
